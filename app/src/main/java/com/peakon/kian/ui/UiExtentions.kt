package com.peakon.kian.ui

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

fun EditText.getOnTextChangeObservable(): Observable<String> {
    val textChangeSubject = PublishSubject.create<String>()

    this.addTextChangedListener(object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun afterTextChanged(editable: Editable?) {
            textChangeSubject.onNext(editable.toString())
        }

    })

    return textChangeSubject

}