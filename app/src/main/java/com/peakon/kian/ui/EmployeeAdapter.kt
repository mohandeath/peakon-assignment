package com.peakon.kian.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.peakon.kian.R
import com.peakon.kian.data.models.Employee
import kotlinx.android.synthetic.main.table_row.view.*

class EmployeeAdapter(
    private val context: Context
) : RecyclerView.Adapter<EmployeeAdapter.EmployeeRowViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmployeeRowViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.table_row, parent, false)
        return EmployeeRowViewHolder(view)
    }

    fun setItems(items: List<Employee>) {
        this.employees.clear()
        this.employees.addAll(items)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return employees.size
    }

    override fun onBindViewHolder(holder: EmployeeRowViewHolder, position: Int) {
        val item = employees[position]
        holder.bindEmployee(item)
    }

    private var employees: MutableList<Employee> = ArrayList()


    inner class EmployeeRowViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bindEmployee(employee: Employee) {
            itemView.tvAge.text = employee.age
            itemView.tvDepartment.text = employee.department
            itemView.tvEmail.text = employee.account?.email ?: "no email"
            itemView.tvManager.text = employee.manager
            itemView.tvBusinessUnit.text = employee.businessUnit
            itemView.tvGender.text = employee.gender
            itemView.tvEmploymentStart.text = employee.employmentStart
            itemView.tvFullName.text = employee.name
            itemView.tvId.text = employee.id.toString()
            itemView.tvJobLevel.text = employee.jobLevel
            itemView.tvLocalOffice.text = employee.localOffice
            itemView.tvSalary.text = "€ ${employee.salary}"
            itemView.tvBonus.text = "€ ${employee.lastYearBonus}"
            itemView.tvPercentageTarget.text = employee.percentageTarget.toString()

        }
    }
}