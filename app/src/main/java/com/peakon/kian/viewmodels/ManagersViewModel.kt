package com.peakon.kian.viewmodels

import android.view.View
import androidx.lifecycle.MutableLiveData
import com.peakon.kian.data.models.Employee
import com.peakon.kian.data.repository.ManagersRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ManagersViewModel @Inject constructor(
    private val repository: ManagersRepository
) : BaseViewModel() {
    val managers = MutableLiveData<List<Employee>>().apply { value = ArrayList() }
    val loadingVisibility = MutableLiveData<Int>().apply { value = View.GONE }
    val retryVisibility = MutableLiveData<Int>().apply { value = View.GONE }
    val errors = MutableLiveData<String>()


    var query: String = ""
        set(value) {
            field = value
            loadEmployees(value)
        }

    fun loadEmployees(query: String) {
        loadingVisibility.value = View.VISIBLE
        repository.getFilteredManagersList(query)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    managers.value = it
                    loadingVisibility.value = View.GONE
                    retryVisibility.value = View.GONE
                }, { error ->
                    error.printStackTrace()
                    loadingVisibility.value = View.GONE
                    retryVisibility.value = View.VISIBLE
                    errors.value = error.message

                })
            .also { addToUnsubscribe(it) }
    }

    fun retry() {
        loadEmployees(query)
    }


}