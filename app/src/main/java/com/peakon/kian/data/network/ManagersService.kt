package com.peakon.kian.data.network

import com.peakon.kian.data.models.Employee
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Url

interface ManagersService {
    /**
     * retrieving the managers information from server
     *
     */
    @GET
    fun retrieveManagersInformation(@Url url: String): Single<List<Employee>>
}