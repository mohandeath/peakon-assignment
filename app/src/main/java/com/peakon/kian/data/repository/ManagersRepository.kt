package com.peakon.kian.data.repository

import com.peakon.kian.common.API_URL
import com.peakon.kian.data.models.Employee
import com.peakon.kian.data.network.ManagersService
import io.reactivex.Single
import javax.inject.Inject


/**
 * since we just have one single source of the data, i've decided to skip implementing
 * a DataSource to keep the architecture well balanced for the scale of project.
 * However, if we had more than one source of data or even more complex CRUD actions,
 * i'd go for implementing IDataSource responsible for that.
 */
class ManagersRepository @Inject constructor(
    private val service: ManagersService
) {

    private fun getManagersList(): Single<List<Employee>> {
        return service.retrieveManagersInformation(API_URL)
    }


    fun getFilteredManagersList(query: String): Single<List<Employee>> {
        return getManagersList().map {
            it.filter { employee ->
                val email = employee.account?.email ?: ""
                employee.name.contains(query, ignoreCase = true) || email.contains(
                    query,
                    ignoreCase = true
                )
            }
        }
    }
}