package com.peakon.kian.data.models


/**
 * Our Domain Model Employee, This is the model we should use.
 * this way helps the application work independent with what's going on
 * in the response. thanks to retrofit's converter we're able to convert the response
 * DTO to a domain model that's more meaningful for the client.
 */
data class Employee(
    val id: Int,
    val manager: String,
    val name: String,
    val employmentStart: String,
    val lastYearBonus: Int,
    val businessUnit: String?,
    val age: String?,
    val department: String?,
    val gender: String?,
    val jobLevel: String?,
    val localOffice: String?,
    val percentageTarget: Int?,
    val region: String?,
    val salary: Int,

    var account: Account? = null


)