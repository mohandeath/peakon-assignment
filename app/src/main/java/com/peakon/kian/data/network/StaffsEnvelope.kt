package com.peakon.kian.data.network

import com.peakon.kian.data.dto.*


data class StaffsEnvelope(

    val data: List<EmployeeDTO>,
    val included: List<EmployeeDTO>

)