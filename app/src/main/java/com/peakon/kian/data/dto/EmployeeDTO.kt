package com.peakon.kian.data.dto

import com.google.gson.annotations.SerializedName
import com.peakon.kian.data.models.Account

open class GenericModel {
    var type: String = ""
    var id: Int = -1
}

data class MetaData(
    val data: GenericModel
)

data class EmployeeDTO(
    val attributes: Attributes,
    val relationships: RelationShips?

) : GenericModel()


data class RelationShips(
    val account: MetaData?,
    val company: MetaData?,
    @SerializedName("Manager") val manager: MetaData?


)