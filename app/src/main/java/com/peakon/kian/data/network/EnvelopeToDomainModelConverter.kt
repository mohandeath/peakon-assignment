package com.peakon.kian.data.network

import com.google.gson.reflect.TypeToken
import com.peakon.kian.data.dto.EmployeeDTO
import com.peakon.kian.data.models.Account
import com.peakon.kian.data.models.Employee
import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Retrofit
import java.lang.reflect.Type
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * i wrote this converter to attach account's to the employee and combined them
 * to a concrete domain model
 * ➜ I've tried to eliminate unnecessary attributes for now
 * ➜since i'm not sure  how these objects will relate in the future,
 * in these cases i usually let the direction of the project lead this in future..
 */
class EnvelopeToDomainModelConverter : Converter.Factory() {

    override fun responseBodyConverter(
        type: Type,
        annotations: Array<Annotation>,
        retrofit: Retrofit
    ): Converter<ResponseBody, List<Employee>>? {


        val envelopeType = TypeToken.getParameterized(StaffsEnvelope::class.java, type).type
        val delegate: Converter<ResponseBody, StaffsEnvelope> =
            retrofit.nextResponseBodyConverter(this, envelopeType, annotations)


        return Converter { body ->
            val result = mutableListOf<Employee>()

            delegate.convert(body)?.let { envelope ->

                // because we have employee Type in included section also a duplicate item appeared in included section,
                val totalList =
                    envelope.data.union(envelope.included).distinctBy { Pair(it.id, it.type) }

                val accounts = totalList.filter { it.type == "accounts" }
                val employees = totalList.filter { it.type == "employees" }

                employees.forEach { dto ->

                    result.add(mapDtoToDomainEmployeeModel(dto, employees, accounts))


                }
            }
            return@Converter result

        }

    }

    private fun mapDtoToDomainEmployeeModel(
        dto: EmployeeDTO,
        employees: List<EmployeeDTO>,
        accounts: List<EmployeeDTO>
    ): Employee {


        val employee = Employee(
            dto.id,
            getManagerName(dto, employees),
            dto.attributes.name,
            dto.attributes.employmentStart,
            dto.attributes.lastYearBonus,
            dto.attributes.businessUnit,
            calculateAge(dto),
            dto.attributes.department,
            dto.attributes.gender,
            dto.attributes.jobLevel,
            dto.attributes.localOffice,
            dto.attributes.percentageTarget,
            dto.attributes.region,
            dto.attributes.salary
        )


        val account = accounts.find { meta ->
            meta.id == dto.relationships?.account?.data?.id
        }

        account?.let { acc ->
            employee.account =
                Account(account.attributes.email, account.attributes.locale)
        }

        return employee

    }


    private fun getAge(dobString: String): Int {

        var date: Date? = null
        val sdf = SimpleDateFormat("yyyy-mm-dd", Locale.getDefault())
        try {
            date = sdf.parse(dobString)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        if (date == null) return 0

        val dob = Calendar.getInstance()
        val today = Calendar.getInstance()

        dob.time = date

        val year = dob.get(Calendar.YEAR)
        val month = dob.get(Calendar.MONTH)
        val day = dob.get(Calendar.DAY_OF_MONTH)

        dob.set(year, month + 1, day)

        var age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR)

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--
        }



        return age
    }

    private fun calculateAge(dto: EmployeeDTO) =
        if (dto.attributes.age != null) getAge(dto.attributes.age!!).toString() else "Unknown"


    private fun getManagerName(dto: EmployeeDTO, employees: List<EmployeeDTO>) =
        employees.find { it.id == dto.relationships?.manager?.data?.id }?.attributes?.name
            ?: "Unknown"

}

