package com.peakon.kian.data.models

data class Account(

    val email: String?,
    val locale: String?
)