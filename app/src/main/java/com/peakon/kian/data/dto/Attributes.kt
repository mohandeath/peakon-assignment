package com.peakon.kian.data.dto

import com.google.gson.annotations.SerializedName

data class Attributes(
    @SerializedName("identifier") val identifier: String,
    @SerializedName("firstName") val firstName: String,
    @SerializedName("lastName") val lastName: String,
    @SerializedName("name") val name: String,
    @SerializedName("features") val features: List<String>,
    @SerializedName("avatar") val avatar: String,
    @SerializedName("employmentStart") val employmentStart: String,
    @SerializedName("external") val external: Boolean,
    @SerializedName("Last Year Bonus") val lastYearBonus: Int,
    @SerializedName("Business Unit") val businessUnit: String,
    @SerializedName("Commute Time") val commuteTime: Int,
    @SerializedName("Age") var age: String?,
    @SerializedName("Department") val department: String,
    @SerializedName("Gender") val gender: String,
    @SerializedName("Job Level") val jobLevel: String,
    @SerializedName("Local Office") val localOffice: String,
    @SerializedName("% of target") val percentageTarget: Int,
    @SerializedName("Region") val region: String,
    @SerializedName("Salary") val salary: Int,
    @SerializedName("Tenure") val tenure: String,
    @SerializedName("email") val email: String = "",
    @SerializedName("locale") val locale: String = ""


)