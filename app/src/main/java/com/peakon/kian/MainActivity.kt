package com.peakon.kian

import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.peakon.kian.di.DaggerViewModelFactory
import com.peakon.kian.ui.EmployeeAdapter
import com.peakon.kian.ui.getOnTextChangeObservable
import com.peakon.kian.viewmodels.ManagersViewModel
import dagger.android.support.DaggerAppCompatActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item_loading.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity() {
    @Inject
    lateinit var viewModelFactory: DaggerViewModelFactory
    private lateinit var viewModel: ManagersViewModel
    private lateinit var layoutManager: RecyclerView.LayoutManager
    private lateinit var adapter: EmployeeAdapter
    private val disposables = CompositeDisposable()




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupViews()
        setupViewModel()
        setupSearchView()
        observeChanges()

    }

    override fun onDestroy() {
        disposables.clear()
        super.onDestroy()
    }

    private fun setupViewModel() {
        viewModel =
            ViewModelProviders.of(this, viewModelFactory).get(ManagersViewModel::class.java)
        viewModel.query = "" //loading all the data

    }


    /**
     *  used Rx to reduce irrelevant calls, this can saves lots of
     *  unnecessary calls to repository and it will be really helpful
     *  specially in case that we want to make searches from a network calls in a real-world app
     */
    private fun setupSearchView() {
        searchBox.getOnTextChangeObservable()
            .debounce(10, TimeUnit.MILLISECONDS)
            .distinctUntilChanged()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { query ->
                viewModel.query = query
            }.addTo(disposables)
    }

    private fun setupViews() {
        adapter = EmployeeAdapter(this)
        layoutManager = LinearLayoutManager(this)
        employeeRecyclerView.adapter = adapter
        employeeRecyclerView.layoutManager = layoutManager
        val decoration = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        decoration.setDrawable(ContextCompat.getDrawable(this,R.drawable.divider)!!)
        employeeRecyclerView.addItemDecoration(decoration)

        retryBtn.setOnClickListener {
            viewModel.retry()
        }

    }

    private fun observeChanges() {
        viewModel.managers.observe(this, Observer {

            adapter.setItems(it)
            employeeRecyclerView.smoothScrollToPosition(0)

        })

        viewModel.loadingVisibility.observe(this, Observer {
            loading.visibility = it!!

        })

        viewModel.retryVisibility.observe(this, Observer {
            retryBtn.visibility = it!!
        })

        viewModel.errors.observe(this, Observer {
            Snackbar.make(
                employeeRecyclerView,
                it,
                Snackbar.LENGTH_LONG
            ).show()

        })

    }


}
