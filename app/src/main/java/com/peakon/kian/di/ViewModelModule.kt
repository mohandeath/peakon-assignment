package com.peakon.kian.di


import androidx.lifecycle.ViewModel
import com.peakon.kian.viewmodels.ManagersViewModel

import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
internal abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(ManagersViewModel::class)
    internal abstract fun provideManagersDataViewModel(viewModel: ManagersViewModel): ViewModel


}
