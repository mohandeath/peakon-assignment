package com.peakon.kian.di

import android.app.Application
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.peakon.kian.common.API_URL
import com.peakon.kian.data.network.EnvelopeToDomainModelConverter
import com.peakon.kian.data.network.ManagersService
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 * This module provides all the app level and generic dependencies, like app context, networking, db, etc.
 */
@Module
class AppModule(private val application: Application) {

    @Singleton
    @Provides
    internal fun providesApplication(): Application {
        return application
    }

    @Singleton
    @Provides
    internal fun provideManagersService(
        gson: Gson,
        httpClient: OkHttpClient
    ): ManagersService {
        val retrofit = Retrofit.Builder()
            .baseUrl(API_URL) //there's no base url! it's a testwork!
            .addConverterFactory(EnvelopeToDomainModelConverter()) // Converting the response Envelop to domain models
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(httpClient)
            .build()

        return retrofit.create(ManagersService::class.java)
    }

    @Provides
    internal fun provideGson(): Gson {

        return GsonBuilder()
            .create()
    }


    @Provides
    internal fun provideHttpClient(): OkHttpClient {

        return OkHttpClient.Builder()
            .build()
    }


}
