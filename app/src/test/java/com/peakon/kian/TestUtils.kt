package com.peakon.kian

import com.peakon.kian.data.models.Account
import com.peakon.kian.data.models.Employee
import junit.framework.Assert

fun assertEqualsEmployeeLists(
    expectedEmployeeList: List<Employee>,
    actualEmployeeList: List<Employee>
) {
    expectedEmployeeList.forEachIndexed { index, expected ->
        val actual = actualEmployeeList[index]

        Assert.assertEquals(expected.id, actual.id)
    }

}

/**
 * populates a list of unique employees with unique name,id,email to check the ability of filtering the results
 */
fun getMockEmployeeList(): List<Employee> {
    val list = arrayListOf<Employee>()
    for (x in 2..20) {
        list.add(
            Employee(
                x,
                "whatever",
                "Test${x}${x * 2}Employee",
                "whatever",
                10,
                null,
                "19",
                "IT",
                "MALE",
                "whatever",
                "whatever",
                10,
                "whatever",
                100,
                Account("whatever${x - 1}${x * 22}@gmail.com", "whatever")
            )
        )
    }
    return list
}

