package com.peakon.kian.network

import android.annotation.SuppressLint
import com.google.gson.GsonBuilder
import com.peakon.kian.BaseTest
import com.peakon.kian.data.models.Employee
import com.peakon.kian.data.network.EnvelopeToDomainModelConverter
import com.peakon.kian.data.network.ManagersService
import junit.framework.Assert.assertEquals
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okio.Okio
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.nio.charset.StandardCharsets

class ManagersServiceTest : BaseTest() {
    private lateinit var service: ManagersService
    private lateinit var mockWebServer: MockWebServer


    @Before
    @Throws(IOException::class)
    fun createService() {
        mockWebServer = MockWebServer()

        val gson = GsonBuilder().create()

        service = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addConverterFactory(EnvelopeToDomainModelConverter()) // Converting the response Envelop to domain models
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(ManagersService::class.java)

    }


    @Throws(IOException::class)
    private fun enqueueResponse(fileName: String) {
        enqueueResponse(fileName, HashMap())
    }

    @SuppressLint("NewApi")
    @Throws(IOException::class)
    private fun enqueueResponse(fileName: String, headers: Map<String, String>) {
        val inputStream = javaClass.classLoader!!
            .getResourceAsStream("responses/$fileName")
        val source = Okio.buffer(Okio.source(inputStream))
        val mockResponse = MockResponse()
        for ((key, value) in headers) {
            mockResponse.addHeader(key, value)
        }
        mockWebServer.enqueue(
            mockResponse
                .setBody(source.readString(StandardCharsets.UTF_8))
        )
    }

    private fun getManagersMockResponse(): List<Employee> {
        enqueueResponse("staff_envelop.json")
        val response = service.retrieveManagersInformation("")
        return response.blockingGet()
    }

    @Test
    fun `Service call should return 10 items in total`() {
        val response = getManagersMockResponse()
        assertEquals(10, response.size) // 9 in data - 1 in included


    }

    @Test
    fun `Service call maps Attributes to the object`() {
        val employee = getManagersMockResponse()[2]
        assertEquals("Mathilda Summers", employee.name)
        assertEquals("43", employee.age)
        assertEquals(95050, employee.lastYearBonus)
        assertEquals("Marketing", employee.businessUnit)
        assertEquals("Media", employee.department)
        assertEquals("Female", employee.gender)
        assertEquals("Executive", employee.jobLevel)
        assertEquals("London", employee.localOffice)
        assertEquals(166, employee.percentageTarget)
        assertEquals(248000, employee.salary)
        assertEquals(142, employee.id)


    }

    @Test
    fun `Service call maps Relations to manager`() {
        val employee = getManagersMockResponse()[2] // Eugene Wong's
        assertEquals("Harriet Banks", employee.manager)

    }

    @Test
    fun `Service call maps Account to the domain model`() {
        val employee = getManagersMockResponse()[3] // Eugene Wong's
        assertEquals("eugene.wong@kinetar.com", employee.account?.email)

        val seccondEmployee = getManagersMockResponse().last() // the extra from included
        assertEquals(null, seccondEmployee.account?.email)
    }


}