package com.peakon.kian.repository

import com.nhaarman.mockitokotlin2.*
import com.peakon.kian.BaseTest
import com.peakon.kian.assertEqualsEmployeeLists
import com.peakon.kian.data.network.ManagersService
import com.peakon.kian.data.repository.ManagersRepository
import com.peakon.kian.getMockEmployeeList
import io.reactivex.Single
import junit.framework.Assert.assertEquals

import org.junit.Before
import org.junit.Test

class ManagersRepositoryTest : BaseTest() {
    private lateinit var repository: ManagersRepository
    private lateinit var managerService: ManagersService

    @Before
    fun init() {
        managerService = mock {
            on { retrieveManagersInformation(any()) } doReturn Single.just(getMockEmployeeList())
        }

        repository = ManagersRepository(managerService)
    }


    @Test
    fun `Repository call  calls the Service`() {
        repository.getFilteredManagersList("")
        verify(managerService, times(1)).retrieveManagersInformation(any())
    }

    @Test
    fun `Repository returns all items when no filters added`() {
        assertEqualsEmployeeLists(
            getMockEmployeeList(),
            repository.getFilteredManagersList("").blockingGet()
        )
    }


    /**
     * I filled the mockList in a way that a mail and a name is compelitely unique with them
     *  these items are the one with the name Test36Employee
     *  and the one with an email of whatever266@gmail.com
     */
    @Test
    fun `Repository should only return one result for selected name query`() {
        assertEquals(1, repository.getFilteredManagersList("Test36").blockingGet().size)
    }

    @Test
    fun `Repository should only return one result for selected email query`() {

        assertEquals(1, repository.getFilteredManagersList("whatever144").blockingGet().size)

    }


}