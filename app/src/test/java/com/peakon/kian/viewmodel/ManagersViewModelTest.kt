@file:Suppress("DEPRECATION")

package com.peakon.kian.viewmodel

import android.view.View
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.*
import com.peakon.kian.BaseTest
import com.peakon.kian.data.repository.ManagersRepository
import com.peakon.kian.getMockEmployeeList
import com.peakon.kian.viewmodels.ManagersViewModel
import io.reactivex.Single
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito

class ManagersViewModelTest : BaseTest() {
    @get:Rule
    val rule = InstantTaskExecutorRule()

    private lateinit var repository: ManagersRepository
    private lateinit var viewModel: ManagersViewModel

    @Before
    fun init() {
        repository = mock {
            on { getFilteredManagersList(any()) } doReturn Single.just(getMockEmployeeList())
        }

        viewModel = ManagersViewModel(repository)
    }


    @Test
    fun `When init it does not call for managers list`() {
        verify(repository, times(0)).getFilteredManagersList(any())

    }

    @Test
    fun `After changing viewmodel query it should call repository again`() {
        viewModel.query = "whatever2"
        verify(repository, times(1)).getFilteredManagersList(eq("whatever2"))

    }

    @Test
    fun `calling retry should call the repo again`() {
        viewModel.retry()
        verify(repository, times(1)).getFilteredManagersList(any())
    }

    @Test
    fun `when init loading is now shown`() {
        assertEquals(View.GONE, viewModel.loadingVisibility.value)
    }


    @Test
    fun `After retry loading should not be shown`() {
        viewModel.retry()
        assertEquals(View.GONE, viewModel.loadingVisibility.value)
    }

    @Test
    fun `After sucessfull call loading is not show`() {
        viewModel.loadEmployees("whatever")
        assertEquals(View.GONE, viewModel.retryVisibility.value)
    }

    @Test
    fun `After sucessfull call retry button is not shown`() {
        viewModel.loadEmployees("whatever")
        assertEquals(View.GONE, viewModel.retryVisibility.value)
    }

    @Test
    fun `After failed call retry is shown`() {
        Mockito.`when`(repository.getFilteredManagersList(any())) doReturn Single.error(
            RuntimeException("Generated SAMPLE Error")
        )


        viewModel.retry()
        assertEquals(View.VISIBLE, viewModel.retryVisibility.value)
    }


}