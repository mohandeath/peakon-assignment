# PEAKON ASSIGNMENT

## how to run the solution:
 -> make debug app : ./gradlew assembleDebug

 -> run tests : ./gradlew test

 ⚠️ however, i recommend to build or run the tests with Android Studio

## Summary

Tried to avoid over engendering and keep the implementation well-balanced for the scale of this solution.
There are some comments in the code that explains my reasoning.



# Technologies
- Kotlin, App is written fully in Kotlin.
- MVVM used for a clean separation of concerns and easy testing.
- RxJava2 is used for reactive programming between layers also delivering a smooth yet optimized search experience
- Dagger2 used for dependency injection
- Retrofit2 for network calls using customized ConverterFactory to parse the response
- Mockito combined with JUnit used for Unit Testing.


## Wishlist:
- Better UI: the tableview may not have a great appearance on every devices and needs more pixel perfection
- Better Size: write dimens.xml for different screen sizes
However, I've make all @diements ready to rewrite for other screen sizes
my focus on this solution was narrowed down into problem-solving. and architecture.







# More information
You can email me at `alirezaakian@gmail.com` if you have any question. Thanks.